"""
Teste para video
"""

import time
import cv2
from openalpr import Alpr
# from def_salva import Salva
# from def_api import Api
import os
import sys


START_TIME = time.time()
VIDEO_SOURCE = '/home/dorathoto/trecis/JetsonNano/top_FullHD.mp4'
WINDOW_NAME = 'openalpr'
FRAME_SKIP = 5
SET_TOP_N = 1
nome_file = ""

def main():
    placas = []
    qtd_placas = 0
    # api = Api('camera_001')
  
    alpr = Alpr("br", "/etc/openalpr/openalpr.conf", "/usr/share/openalpr/runtime_data", "", False,0,5)
    if not alpr.is_loaded():
        print('Erro ao carregar OpenALPR')
        sys.exit(1)

    alpr.set_top_n(SET_TOP_N)
    alpr.motion_detection = 1
    alpr.set_default_region('br')
    alpr.analysis_threads = 4
    alpr.camera_buffer_size  =  60

    cap = cv2.VideoCapture(VIDEO_SOURCE)
    if not cap.isOpened():
        alpr.unload()
        sys.exit('Falha ao carregar o video!')

    _frame_number = 0
    while True:
        ret_val, frame = cap.read()
        if not ret_val:
            print('VidepCapture.read() falhou. Saindo....')
            break

        _frame_number += 1
        if _frame_number % FRAME_SKIP != 0:
            continue
        results = alpr.recognize_ndarray(frame)
        for i, plate in enumerate(results['results']):
            qtd_placas += 1
            melhor_placa = plate['candidates'][0]
            placa = melhor_placa['plate'].upper()

            coord_ini_x = 1919 if plate['coordinates'][0]['x'] > 1920 else plate['coordinates'][0]['x']
            coord_ini_y = plate['coordinates'][0]['y']
            coord_fim_x = 1919 if plate['coordinates'][2]['x'] > 1920 else plate['coordinates'][2]['x']
            coord_fim_y = plate['coordinates'][2]['y']
            if placa not in placas:
                if len(placa) == 7:

                    placas.append(placa)
                    acuracia = melhor_placa['confidence']
                    # save = Salva(placa)
                    # nome_file = save.SalvaFullImage(frame)
                    # crop_placa = frame[coord_ini_y:coord_fim_y, coord_ini_x:coord_fim_x]
                    # save.SalvaCropImage(crop_placa)
                    coordInicial = plate['coordinates'][0]['x'],plate['coordinates'][0]['y']
                    coordFinal = plate['coordinates'][2]['x'],plate['coordinates'][2]['y']
                    frame = cv2.rectangle(frame, coordinicial, coordfinal, (0,255,0), 5)
                    cv2.imshow(window_name, frame)

                    
            
            print('{}.{} - Qtd. Placas {} - {:7s} ({:.2f}%) - {:.2f}ms - {}.jpg'.format(qtd_placas, len(placas), i+1, placa, acuracia, plate['processing_time_ms'],nome_file))
        if cv2.waitKey(1) == 27:
            break

    cv2.destroyAllWindows()
    cap.release()
    alpr.unload()
    print('-------------')
    #print(placas)
    print(" ------ {:2f} segundos ------".format(time.time() - START_TIME))

if __name__ == "__main__":
    main()
